FROM golang:1.18 as builder
WORKDIR /workspace
COPY ./ ./
ARG package=.
RUN CGO_ENABLED=0 GOOS=linux go build -trimpath -ldflags "-extldflags '-static'" -o manager ${package}

FROM gcr.io/distroless/static:nonroot
WORKDIR /
COPY --from=builder /workspace/manager .
# Use uid of nonroot user (65532) because kubernetes expects numeric user when applying pod security policies
USER 65532
ENTRYPOINT ["/manager"]
