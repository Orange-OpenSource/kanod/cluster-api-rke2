#!/bin/bash

if ! command -v virt-copy-in &> /dev/null; then
    echo "libguestfs-tools are required to run this script, please install them"
    exit 1
fi

IMG_URL=https://cloud.debian.org/images/cloud/bullseye/latest/debian-11-genericcloud-amd64.qcow2

RKE2_VERSION=v1.24.3+rke2r1

IMG_NAME=debian-11-rke2-${RKE2_VERSION}

curl -sfL ${IMG_URL} --output ${IMG_NAME}

TMP_DIR=$(mktemp -d)/rke2
mkdir ${TMP_DIR}

curl -OLs https://github.com/rancher/rke2/releases/download/${RKE2_VERSION}/rke2-images.linux-amd64.tar.zst --output-dir ${TMP_DIR}
curl -OLs https://github.com/rancher/rke2/releases/download/${RKE2_VERSION}/rke2.linux-amd64.tar.gz --output-dir ${TMP_DIR}
curl -OLs https://github.com/rancher/rke2/releases/download/${RKE2_VERSION}/sha256sum-amd64.txt --output-dir ${TMP_DIR}
curl -sfL https://get.rke2.io --output ${TMP_DIR}/install.sh

virt-copy-in -a ${IMG_NAME} ${TMP_DIR} /opt/rke2

rm -Rf ${TMP_DIR}

echo "Image ${IMG_NAME} successfully prepared"
