/*
Copyright 2019 The Kubernetes Authors.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package cloudinit

import "fmt"

const (
	controlPlaneCloudJoin = `{{.Header}}
{{template "files" .WriteFiles}}
runcmd:
{{- template "commands" .PreRKE2Commands }}
{{if .DeployRKE2Commands}}
  - export INSTALL_RKE2_VERSION=%[1]s
{{- template "commands" .DeployRKE2Commands }}
{{else}}
  - 'curl -sfL https://get.rke2.io | INSTALL_RKE2_VERSION=%[1]s sh -s - server'
  - 'systemctl enable rke2-server.service'
  - 'systemctl start rke2-server.service'
{{end}}
{{- template "commands" .PostRKE2Commands }}
`
)

// NewInitControlPlane returns the user data string to be used on a controlplane instance.
func NewJoinControlPlane(input *ControlPlaneInput) ([]byte, error) {
	input.Header = cloudConfigHeader
	input.WriteFiles = append(input.WriteFiles, input.AdditionalFiles...)
	input.WriteFiles = append(input.WriteFiles, input.ConfigFile)

	controlPlaneCloudJoinWithVersion := fmt.Sprintf(controlPlaneCloudJoin, input.RKE2Version)
	userData, err := generate("JoinControlplane", controlPlaneCloudJoinWithVersion, input)
	if err != nil {
		return nil, err
	}

	return userData, nil
}
