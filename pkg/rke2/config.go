package rke2

import (
	"fmt"
	"strings"

	bootstrapv1 "gitlab.com/Orange-Opensource/kanod/cluster-api-rke2/bootstrap/api/v1alpha1"
)

const DefaultRKE2ConfigLocation = "/etc/rancher/rke2/config.yaml"
const DefaultRKE2JoinPort = 9345

type RKE2ServerConfig struct {
	DisableCloudController    bool     `json:"disable-cloud-controller,omitempty"`
	KubeAPIServerArgs         []string `json:"kube-apiserver-arg,omitempty"`
	KubeControllerManagerArgs []string `json:"kube-controller-manager-arg,omitempty"`
	TLSSan                    []string `json:"tls-san,omitempty"`
	BindAddress               string   `json:"bind-address,omitempty"`
	HttpsListenPort           string   `json:"https-listen-port,omitempty"`
	AdvertiseAddress          string   `json:"advertise-address,omitempty"`
	AdvertisePort             string   `json:"advertise-port,omitempty"`
	ClusterCidr               string   `json:"cluster-cidr,omitempty"`
	ServiceCidr               string   `json:"service-cidr,omitempty"`
	ClusterDNS                string   `json:"cluster-dns,omitempty"`
	ClusterDomain             string   `json:"cluster-domain,omitempty"`
	DisableComponents         []string `json:"disable,omitempty"`
	ClusterInit               bool     `json:"cluster-init,omitempty"`
	RKE2AgentConfig           `json:",inline"`
}

type RKE2AgentConfig struct {
	Token           string   `json:"token,omitempty"`
	Server          string   `json:"server,omitempty"`
	KubeletArgs     []string `json:"kubelet-arg,omitempty"`
	NodeLabels      []string `json:"node-labels,omitempty"`
	NodeTaints      []string `json:"node-taints,omitempty"`
	PrivateRegistry string   `json:"private-registry,omitempty"`
	KubeProxyArgs   []string `json:"kube-proxy-arg,omitempty"`
	NodeName        string   `json:"node-name,omitempty"`
}

func GenerateInitControlPlaneConfig(controlPlaneEndpoint string, token string, serverConfig bootstrapv1.RKE2ServerConfig, agentConfig bootstrapv1.RKE2AgentConfig) RKE2ServerConfig {
	rke2ServerConfig := RKE2ServerConfig{
		DisableCloudController:    true,
		ClusterInit:               true,
		KubeAPIServerArgs:         append(serverConfig.KubeAPIServerArgs, "anonymous-auth=true", getTlsCipherSuiteArg()),
		TLSSan:                    append(serverConfig.TLSSan, controlPlaneEndpoint),
		KubeControllerManagerArgs: serverConfig.KubeControllerManagerArgs,
		BindAddress:               serverConfig.BindAddress,
		HttpsListenPort:           serverConfig.HttpsListenPort,
		AdvertiseAddress:          serverConfig.AdvertiseAddress,
		AdvertisePort:             serverConfig.AdvertisePort,
		ClusterCidr:               serverConfig.ClusterCidr,
		ServiceCidr:               serverConfig.ServiceCidr,
		ClusterDNS:                serverConfig.ClusterDNS,
		ClusterDomain:             serverConfig.ClusterDomain,
		DisableComponents:         serverConfig.DisableComponents,
	}

	rke2ServerConfig.RKE2AgentConfig = RKE2AgentConfig{
		Token:           token,
		KubeletArgs:     agentConfig.KubeletArgs,
		NodeLabels:      agentConfig.NodeLabels,
		NodeTaints:      agentConfig.NodeTaints,
		PrivateRegistry: agentConfig.PrivateRegistry,
		KubeProxyArgs:   agentConfig.KubeProxyArgs,
		NodeName:        agentConfig.NodeName,
	}

	return rke2ServerConfig
}

func GenerateJoinControlPlaneConfig(serverUrl string, token string, controlplaneendpoint string, serverConfig bootstrapv1.RKE2ServerConfig, agentConfig bootstrapv1.RKE2AgentConfig) RKE2ServerConfig {

	rke2ServerConfig := RKE2ServerConfig{
		DisableCloudController:    true,
		KubeAPIServerArgs:         append(serverConfig.KubeAPIServerArgs, "anonymous-auth=true", getTlsCipherSuiteArg()),
		TLSSan:                    append(serverConfig.TLSSan, controlplaneendpoint),
		KubeControllerManagerArgs: serverConfig.KubeControllerManagerArgs,
		BindAddress:               serverConfig.BindAddress,
		HttpsListenPort:           serverConfig.HttpsListenPort,
		AdvertiseAddress:          serverConfig.AdvertiseAddress,
		AdvertisePort:             serverConfig.AdvertisePort,
		ClusterCidr:               serverConfig.ClusterCidr,
		ServiceCidr:               serverConfig.ServiceCidr,
		ClusterDNS:                serverConfig.ClusterDNS,
		ClusterDomain:             serverConfig.ClusterDomain,
		DisableComponents:         serverConfig.DisableComponents,
	}

	rke2ServerConfig.RKE2AgentConfig = RKE2AgentConfig{
		Token:           token,
		Server:          serverUrl,
		KubeletArgs:     agentConfig.KubeletArgs,
		NodeLabels:      agentConfig.NodeLabels,
		NodeTaints:      agentConfig.NodeTaints,
		PrivateRegistry: agentConfig.PrivateRegistry,
		KubeProxyArgs:   agentConfig.KubeProxyArgs,
		NodeName:        agentConfig.NodeName,
	}

	return rke2ServerConfig
}

func GenerateWorkerConfig(serverUrl string, token string, agentConfig bootstrapv1.RKE2AgentConfig) RKE2AgentConfig {
	return RKE2AgentConfig{
		Server:          serverUrl,
		Token:           token,
		KubeletArgs:     agentConfig.KubeletArgs,
		NodeLabels:      agentConfig.NodeLabels,
		NodeTaints:      agentConfig.NodeTaints,
		PrivateRegistry: agentConfig.PrivateRegistry,
		KubeProxyArgs:   agentConfig.KubeProxyArgs,
		NodeName:        agentConfig.NodeName,
	}
}

func getTlsCipherSuiteArg() string {

	/**
	FIXME(feleouet): revisit that in rke2 context

	Can't use this method because rke2 is using older apiserver pkgs that hardcode a subset of ciphers.
	https://github.com/rke2-io/rke2/blob/master/vendor/k8s.io/component-base/cli/flag/ciphersuites_flag.go#L29
	csList := ""
	css := tls.CipherSuites()
	for _, cs := range css {
		csList += cs.Name + ","
	}
	csList = strings.TrimRight(csList, ",")
	**/

	ciphers := []string{
		// Modern Compatibility recommended configuration in
		// https://wiki.mozilla.org/Security/Server_Side_TLS#Modern_compatibility
		"TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384",
		"TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384",
		"TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256",
		"TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256",
		"TLS_ECDHE_ECDSA_WITH_CHACHA20_POLY1305",
		"TLS_ECDHE_RSA_WITH_CHACHA20_POLY1305",

		// Few Intermediate Compatibility ones to support AWS ELB, that don't support forward secrecy.
		// note: golang tls library doesn't support all intermediate cipher suites.
		"TLS_RSA_WITH_AES_128_GCM_SHA256",
		"TLS_RSA_WITH_AES_256_GCM_SHA384",
	}

	ciphersList := ""
	for _, cc := range ciphers {
		ciphersList += cc + ","
	}
	ciphersList = strings.TrimRight(ciphersList, ",")

	return fmt.Sprintf("tls-cipher-suites=%s", ciphersList)
}
