# Cluster API rke2

This project provides a boostrap and control plane provider for rke2, they enable the deployment of rke2 clusters with [Cluster API](https://github.com/kubernetes-sigs/cluster-api/blob/master/README.md)

It is derived from [cluster API k3s provider](https://github.com/zawachte/cluster-api-k3s) that has been adapted to allow deployment of RKE2 clusters instead of k3s

## Testing it out.

Assuming that you have docker, kubectl with access to a cluster, and a registry to push images to, you may deploy these controllers using provided manifests and images:

```
kubectl apply -k bootstrap/config/default
kubectl apply -k controlplane/config/default
```

Alternatively, you may use makefile to build and publish images locally

```
REGISTRY=your.registry.url make
```

Once installed, you may refer to [provided samples](./samples) that illustrate the use of this provider alongside with openstack instructure provider. They may be used in [capo-bootstrap](https://gitlab.com/t6306/components/capi-bootstrap) as an alternative cluster definition.

By default, RKE2 can be installed on any image, and it will use [typical RKE2 deployment method](https://docs.rke2.io/install/quickstart/#server-node-installation) that requires access to internet.

As internet connectivity may not be available, [alternative air-gap deployment options](https://docs.rke2.io/install/airgap/) may be used. For that purpose, you may provide an image preloaded with required binaries, or a private registry that provides rancher images, and sepcify the set of commands to issue to deploy RKE2 using `DeployRKE2Commands` parameters.

As an example, we provide [preload-images.sh](./samples/preload-images.sh) script that builds an image following [install.sh method](https://docs.rke2.io/install/airgap/#rke2-installsh-script-install)

This image can then be used with following commands (as detailled in [offline-rke2-cluster.yaml](./samples/offline-rke2-cluster.yaml))

```
---
apiVersion: controlplane.cluster.x-k8s.io/v1beta1
kind: RKE2ControlPlane
spec:
  [...]
  rke2ConfigSpec:
    deployRKE2Commands:
      - INSTALL_RKE2_ARTIFACT_PATH=/opt/rke2 sh /opt/rke2/install.sh - server
      - systemctl enable rke2-server.service
      - systemctl start rke2-server.service
```

### Changes over cluster-api-k3s

- sed s/k3s/rke2/g
- Change the commands used to bootstrap nodes (obviously)
- Change port used (rke2 uses a dedicated port 9345)
- Add an deployRKE2Commands option to enable user to provide custom commands to perform air-gapped deployments
- Remove cloud-provider=external flag from kubelet and kube-controller-manager to prevent nodes from being tainted

### Known Issues

* Nodes shoud be redeployed when RKE2ConfigTemplate is updated
* Version may be ignored when rke2 is deployed from tarball...
* ClusterCIDR/ServiceCIDR/ClusterDomain variables from capi cluster should be honored
* Revisit serverConfig/agentConfig in rke2ConfigSpec (first should be an superset of second one?)
* If init infra-machine get deleted and rebuild by infra provider at some point, it will be re-created with cluster-init=true, and build a new (split-brain) cluster
* Nodes sould be deleted when machines are deleted, otherwise control plane get lost during rolling upgrades and deletes too much nodes...
