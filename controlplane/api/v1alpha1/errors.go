package v1alpha1

type RKE2ControlPlaneStatusError string

const (
	// InvalidConfigurationRKE2ControlPlaneError indicates that the RKE2 control plane
	// configuration is invalid.
	InvalidConfigurationRKE2ControlPlaneError RKE2ControlPlaneStatusError = "InvalidConfiguration"

	// UnsupportedChangeRKE2ControlPlaneError indicates that the RKE2 control plane
	// spec has been updated in an unsupported way that cannot be
	// reconciled.
	UnsupportedChangeRKE2ControlPlaneError RKE2ControlPlaneStatusError = "UnsupportedChange"

	// CreateRKE2ControlPlaneError indicates that an error was encountered
	// when trying to create the RKE2 control plane.
	CreateRKE2ControlPlaneError RKE2ControlPlaneStatusError = "CreateError"

	// UpdateRKE2ControlPlaneError indicates that an error was encountered
	// when trying to update the RKE2 control plane.
	UpdateRKE2ControlPlaneError RKE2ControlPlaneStatusError = "UpdateError"

	// DeleteRKE2ControlPlaneError indicates that an error was encountered
	// when trying to delete the RKE2 control plane.
	DeleteRKE2ControlPlaneError RKE2ControlPlaneStatusError = "DeleteError"
)
